## How to install a list of packages

Use pacman to install a list of packages from a file.

    sudo pacman -S --input package_list.txt
