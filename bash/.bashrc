# If not running interactively, don't do anything.
# ************************************************
[[ $- != *i* ]] && return

git_prompt="$HOME/.config/panarcadia/git/git-prompt.sh"
git_completion="$HOME/.config/panarcadia/git/git-completion.bash"

# Source git-completion if it's there.
# Used for tab completion of git commands.
# ****************************************
[ -f "$git_completion" ] && source "$git_completion"

# Set up and export the PS1 variable.
# Add git repository status, if it's available.
# *********************************************
if [ -f "$git_prompt" ]; then
    # Store prompt with git information.
    source "$git_prompt"
    export GIT_PS1_SHOWDIRTYSTATE=1
    export GIT_PS1_SHOWUNTRACKEDFILES=1
    export GIT_PS1_SHOWUPSTREAM="auto verbose"

    green="\[$(tput setaf 2)\]"
    yellow="\[$(tput setaf 3)\]"
    blue="\[$(tput setaf 4)\]"
    white="\[$(tput setaf 7)\]"
    reset="\[$(tput sgr0)\]"
    bold="\[$(tput bold)\]"
    git='$(__git_ps1 " (%s)")'

    PS1="${bold}${blue}\u@\h ${green}[\j] ${blue}\w${yellow}${git}${white}\n${reset}-> "
else
    # Store prompt without git information.
    PS1="${bold}${blue}\u@\h ${green}[\j] ${blue}\w${reset}\n-> "
fi

export PS1

# Aliases
# *******
# -A, --almost-all
# -F, --classify (append indicator to entry */=>@!)
# -h, --human-readable
# -l, --format=long
# -v, natural sort of numbers
# List storage using long list format (exclude hidden files).
alias ll="ls -Fhlv --group-directories-first --color=auto"

# List storage using long list format (include hidden files).
alias lla="ls -AFhlv --group-directories-first --color=auto"

# List storage using long list format (only hidden files).
alias llh="ls -Al --ignore='*'"

# List storage (exclude hidden files).
alias ls="ls -v --group-directories-first --color=auto"

# List storage (include hidden files).
alias lsa="ls -Av --group-directories-first --color=auto"

alias lsh="ls -A --ignore='*'"

# Use color when grepping.
alias grep='grep --color=auto'

# All files (include hidden), use color always, ignore .git directory.
alias tree="tree -aC --dirsfirst -I .git"

# Aliases for changing to commonly used directories.
# **************************************************
alias cdbin="cd $HOME/bin"
alias cdconfig="cd $HOME/.config/panarcadia"
alias cdnvim="cd $HOME/.config/nvim"
alias cdprojects="cd $HOME/projects"
alias cdzet="cd $HOME/zet"
alias cdpannotes="cd $HOME/pannotes"

# ignorespace and ignoredups.
# ***************************
export HISTCONTROL=ignoreboth:erasedups

# Add blank line between prompts.
# *******************************
# PROMPT_COMMAND="echo;$PROMPT_COMMAND"

# Add $HOME/bin to PATH.
# Test first if bin is already in $PATH.
# **************************************
bin_path="$HOME/bin"
[[ ":$PATH:" =~ ":$bin_path:" ]] || PATH="$bin_path:$PATH"

# ****
# EOF
# ****
