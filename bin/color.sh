#! /usr/bin/env bash

##
#   BASH menu script that checks:
#       - Memory usage
#       - CPU load
#       - Kernel version
##

##
#   Color Variables
##
green='\e[32m'
blue='\e[34m'
clear='\e[0m'

##
#   Color Functions
##
ColorGreen() {
    echo -ne $green$1$clear
}

ColorBlue() {
    echo -ne $blue$1$clear
}

server_name=$(hostname)

function memory_check() {
    echo ""
    echo "Memory usage on $server_name is: "
    free -h
    echo ""
}

function cpu_check() {
    echo ""
    echo "CPU load on $server_name is: "
    uptime
    echo ""
}

function kernel_check() {
    echo ""
    echo "Kernel version on $server_name is: "
    uname -r
    echo ""
}

function all_checks() {
    memory_check
    cpu_check
    kernel_check
}

all_checks
