#!/usr/bin/env bash

# Warning:
# ********
# This script will remove config files existing on the machine.
# Back up anything you do not want to lose.
# ********

# Clang
fname=".clang-format"
rm "$HOME/$fname" 2> /dev/null
ln -s "$HOME/.config/panarcadia/clang/$fname" "$HOME/$fname"

# .bashrc
fname=".bashrc"
rm "$HOME/$fname" 2> /dev/null
ln -s "$HOME/.config/panarcadia/bash/$fname" "$HOME/$fname"

# Git
dir_name="git"
rm -rf "$HOME/.config/$dir_name" 2> /dev/null
ln -s "$HOME/.config/panarcadia/$dir_name" "$HOME/.config/$dir_name"

# bin
dir_name="bin"
rm "$HOME/$dir_name" 2> /dev/null
ln -s "$HOME/.config/panarcadia/$dir_name" "$HOME/$dir_name"

# Tmux
dir_name="tmux"
rm -rf "$HOME/.config/$dir_name" 2> /dev/null
ln -s "$HOME/.config/panarcadia/$dir_name" "$HOME/.config/$dir_name"

# Zed
dir_name="zed"
rm -rf "$HOME/.config/$dir_name" 2> /dev/null
ln -s "$HOME/.config/panarcadia/$dir_name" "$HOME/.config/$dir_name"

# Nano
dir_name="nano"
rm -rf "$HOME/.config/$dir_name" 2> /dev/null
ln -s "$HOME/.config/panarcadia/$dir_name" "$HOME/.config/$dir_name"
